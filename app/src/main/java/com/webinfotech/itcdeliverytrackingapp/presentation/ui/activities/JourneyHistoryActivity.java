package com.webinfotech.itcdeliverytrackingapp.presentation.ui.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.itcdeliverytrackingapp.AndroidApplication;
import com.webinfotech.itcdeliverytrackingapp.R;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.JourneyHistoryPresenter;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.impl.JourneyHistoryPresenterImpl;
import com.webinfotech.itcdeliverytrackingapp.presentation.ui.services.LocationService;
import com.webinfotech.itcdeliverytrackingapp.threading.MainThreadImpl;
import com.webinfotech.itcdeliverytrackingapp.util.GetLocationClass;
import com.webinfotech.itcdeliverytrackingapp.util.Helper;

import java.io.IOException;
import java.util.List;

public class JourneyHistoryActivity extends AppCompatActivity implements JourneyHistoryPresenter.View, GetLocationClass.Callback {

    @BindView(R.id.txt_view_distance)
    TextView txtViewDistance;
    private BroadcastReceiver broadcastReceiver;
    String[] appPremisions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    List<Address> addresses;
    ProgressDialog progressDialog;
    JourneyHistoryPresenterImpl mPresenter;
    GetLocationClass getLocationClass;
    String km;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey_history);
        getSupportActionBar().setTitle("End Journey");
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        initialiseLocationClass();
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new JourneyHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void initialiseLocationClass() {
        getLocationClass = new GetLocationClass(this, this::onLocationGettingSuccess);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }


    @OnClick(R.id.btn_end_journey) void onEndJourneyClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmation !");
        builder.setMessage("Are You Sure ?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                km = txtViewDistance.getText().toString();
                showLoader();
                AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
                androidApplication.setDistanceTravelledDistanceTo(getApplicationContext(), 0, false);
                Intent i = new Intent(getApplicationContext(), LocationService.class);
                stopService(i);
                getLocationClass.getCurrentLocation();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (broadcastReceiver ==  null) {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    hideLoader();
                    AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
                    Log.e("LogMsg", Integer.toString(androidApplication.getKm(getApplicationContext())));
                    float i = (float) androidApplication.getKm(getApplicationContext()) / 1000;
                    txtViewDistance.setText(Float.toString(i));
                }
            };
        }
        registerReceiver(broadcastReceiver, new IntentFilter("Location Update"));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }


    @Override
    public void onEndJourneySuccess() {
        Toast.makeText(this, "Journey Ended Successfully", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, JourneyStartActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {

            unregisterReceiver(broadcastReceiver);
            //Register or UnRegister your broadcast receiver here

        } catch(IllegalArgumentException e) {

            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {

            unregisterReceiver(broadcastReceiver);
            //Register or UnRegister your broadcast receiver here

        } catch(IllegalArgumentException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onLocationGettingSuccess(Location location) {
        getLocationClass.stopLocationUpdates();
        try {
            List<Address> addressList = Helper.getAddressFromLatLang(this, location);
            String address = addressList.get(0).getAddressLine(0) + ", " + addressList.get(0).getLocality() + ", " + addressList.get(0).getAdminArea();
            mPresenter.endJourney(Double.toString(location.getLatitude()), Double.toString(location.getLongitude()), address, km);
        } catch (IOException e) {
            hideLoader();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}
