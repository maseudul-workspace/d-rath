package com.webinfotech.itcdeliverytrackingapp.presentation.ui.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.util.Log;

import com.webinfotech.itcdeliverytrackingapp.AndroidApplication;
import com.webinfotech.itcdeliverytrackingapp.util.GetLocationClass;

import androidx.annotation.Nullable;

public class LocationService extends Service implements GetLocationClass.Callback {

    GetLocationClass getLocationClass;
    Location oldLocation;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        getLocationClass = new GetLocationClass(this, this);
        getLocationClass.getCurrentLocation();
        Log.e("LogMsg", "Service Created");
        return START_STICKY;
    }

    @Override
    public void onLocationGettingSuccess(Location location) {
        try {
            AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
            int storedKm = androidApplication.getKm(this);
            storedKm = storedKm + (int) calculateDistanceByDistanceTo(oldLocation, location);
            androidApplication.setDistanceTravelledDistanceTo(this, storedKm, true);
            Log.e("LogMsg", "Distance: " + storedKm);
            Intent i = new Intent("Location Update");
            sendBroadcast(i);
        } catch (NullPointerException e) {

        }
        oldLocation = location;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("LogMsg", "Destroyed: ");
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        if (androidApplication.getStatus(this)) {
            Intent i = new Intent("RestartService");
            i.putExtra("startService", true);
            i.setClass(this, RestartService.class);
            sendBroadcast(i);
        } else {
            getLocationClass.stopLocationUpdates();
        }
    }

    public double calculateDistanceByDistanceTo(Location locationA, Location locationB){
        double meter = locationA.distanceTo(locationB);
        return meter;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceIntent = new Intent(getApplicationContext(),this.getClass());
        restartServiceIntent.setPackage(getPackageName());
        startService(restartServiceIntent);
        super.onTaskRemoved(rootIntent);
    }

}
