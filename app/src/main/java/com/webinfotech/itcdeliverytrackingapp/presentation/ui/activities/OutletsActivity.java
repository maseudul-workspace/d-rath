package com.webinfotech.itcdeliverytrackingapp.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

import com.webinfotech.itcdeliverytrackingapp.R;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.OutletsPresenter;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.impl.OutletsPresenterImpl;
import com.webinfotech.itcdeliverytrackingapp.presentation.ui.adapters.OutletAdapter;
import com.webinfotech.itcdeliverytrackingapp.threading.MainThreadImpl;
import com.webinfotech.itcdeliverytrackingapp.util.GetLocationClass;
import com.webinfotech.itcdeliverytrackingapp.util.Helper;

import java.io.IOException;
import java.util.List;

public class OutletsActivity extends AppCompatActivity implements OutletsPresenter.View, GetLocationClass.Callback {

    @BindView(R.id.recycler_view_outlets)
    RecyclerView recyclerViewOutlets;
    OutletsPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    GetLocationClass getLocationClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outlets);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Outlets");
        initialisePresenter();
        initialiseLocationClass();
        setUpProgressDialog();
        showLoader();
        mPresenter.fetchOutletLists();
    }

    public void initialisePresenter() {
        mPresenter = new OutletsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void initialiseLocationClass() {
        getLocationClass = new GetLocationClass(this, this::onLocationGettingSuccess);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }


    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loadAdapter(OutletAdapter adapter) {
        recyclerViewOutlets.setAdapter(adapter);
        recyclerViewOutlets.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewOutlets.setNestedScrollingEnabled(false);
    }

    @Override
    public void onDeliverClicked() {
        getLocationClass.getCurrentLocation();
    }

    @OnClick(R.id.btn_end_journey) void onEndJourneyClicked() {
        Intent intent = new Intent(this, JourneyHistoryActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onLocationGettingSuccess(Location location) {
        getLocationClass.stopLocationUpdates();
        try {
            List<Address> addressList = Helper.getAddressFromLatLang(this, location);
            String address = addressList.get(0).getAddressLine(0) + ", " + addressList.get(0).getLocality() + ", " + addressList.get(0).getAdminArea();
            mPresenter.deliverOutlet(address, Double.toString(location.getLatitude()), Double.toString(location.getLongitude()));
        } catch (IOException e) {
            hideLoader();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}
