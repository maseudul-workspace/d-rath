package com.webinfotech.itcdeliverytrackingapp.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.webinfotech.itcdeliverytrackingapp.R;
import com.webinfotech.itcdeliverytrackingapp.domain.models.OutletList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OutletAdapter extends RecyclerView.Adapter<OutletAdapter.ViewHolder> {

    public interface Callback {
        void onOutletClicked(int outletId);
    }

    Context mContext;
    OutletList[] outletLists;
    Callback mCallback;

    public OutletAdapter(Context mContext, OutletList[] outletLists, Callback mCallback) {
        this.mContext = mContext;
        this.outletLists = outletLists;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_outlets, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txt_view_outlet_name.setText(outletLists[position].name);
        holder.txt_view_address.setText(outletLists[position].address);
        holder.txt_view_ds_name.setText(outletLists[position].dsName);
        holder.txt_view_ds_type.setText(outletLists[position].dsType);
        holder.txt_view_sify_id.setText(outletLists[position].sifyId);
        if (outletLists[position].delStatus == 1) {
            holder.btnDeliver.setVisibility(View.VISIBLE);
            holder.txtViewStatus.setVisibility(View.GONE);
        } else {
            holder.btnDeliver.setVisibility(View.GONE);
            holder.txtViewStatus.setVisibility(View.VISIBLE);
        }
        holder.btnDeliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirmation");
                builder.setMessage("Are You Sure ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onOutletClicked(outletLists[position].outletId);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return outletLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_outlet_name)
        TextView txt_view_outlet_name;
        @BindView(R.id.txt_view_ds_name)
        TextView txt_view_ds_name;
        @BindView(R.id.txt_view_ds_type)
        TextView txt_view_ds_type;
        @BindView(R.id.txt_view_sify_id)
        TextView txt_view_sify_id;
        @BindView(R.id.txt_view_address)
        TextView txt_view_address;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.btn_deliver)
        Button btnDeliver;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
