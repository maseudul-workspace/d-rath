package com.webinfotech.itcdeliverytrackingapp.presentation.presenters;

import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyStartData;
import com.webinfotech.itcdeliverytrackingapp.presentation.ui.adapters.BeatAdapter;
import com.webinfotech.itcdeliverytrackingapp.presentation.ui.adapters.VehicleAdapter;

public interface JourneyStartPresenter {
    void getJourneyFormData();
    void startJourney(int beatId,
                      int vehicleId,
                      String startLatitude,
                      String startLongitude,
                      String startAddress);
    interface View {
        void showLoader();
        void hideLoader();
        void loadData(BeatAdapter beatAdapter, VehicleAdapter vehicleAdapter);
        void onBeatClicked(String beatName, int beatId);
        void onVehicleClicked(String vehicleName, int vehicleId);
        void onJourneyStartSuccess(JourneyStartData journeyStartData);
        void onJourneyStartFail();
    }
}
