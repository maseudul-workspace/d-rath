package com.webinfotech.itcdeliverytrackingapp.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.itcdeliverytrackingapp.AndroidApplication;
import com.webinfotech.itcdeliverytrackingapp.R;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.LoginPresenter;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.impl.LoginPresenterImpl;
import com.webinfotech.itcdeliverytrackingapp.threading.MainThreadImpl;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View
{

    @BindView(R.id.txt_input_phone_layout)
    TextInputLayout txtInputPhoneLayout;
    @BindView(R.id.txt_input_password_layout)
    TextInputLayout txtInputPasswordLayout;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    LoginPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        getSupportActionBar().setTitle("Log In");
    }

    public void initialisePresenter() {
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_login) void onLoginClicked() {
        txtInputPhoneLayout.setError("");
        txtInputPasswordLayout.setError("");
        if (editTextPassword.getText().toString().trim().isEmpty()) {
            txtInputPasswordLayout.setError("Password Required");
        } else if (editTextPhone.getText().toString().trim().isEmpty()) {
            txtInputPhoneLayout.setError("Phone No Required");
        } else {
            mPresenter.checkLogin(editTextPhone.getText().toString(), editTextPassword.getText().toString());
            showLoader();
        }
    }

    @Override
    public void onLoginSuccess() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        if (androidApplication.getStatus(this)) {

        } else {
            goToJourneyStartActivity();
        }
    }

    public void goToJourneyStartActivity() {
        Intent intent = new Intent(this, JourneyStartActivity.class);
        startActivity(intent);
        finish();
    }

}
