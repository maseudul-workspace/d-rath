package com.webinfotech.itcdeliverytrackingapp.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.itcdeliverytrackingapp.R;
import com.webinfotech.itcdeliverytrackingapp.domain.models.Beat;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BeatAdapter extends RecyclerView.Adapter<BeatAdapter.ViewHolder> {

    public interface Callback {
        void onBeatClicked(int beatId, String beatName);
    }

    Context mContext;
    Beat[] beat;
    Callback mCallback;

    public BeatAdapter(Context mContext, Beat[] beat, Callback mCallback) {
        this.mContext = mContext;
        this.beat = beat;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_beat_name, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txt_view_beat_name.setText(beat[position].name);
        holder.txt_view_beat_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onBeatClicked(beat[position].id, beat[position].name);
            }
        });
    }

    @Override
    public int getItemCount() {
        return beat.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_beat_name)
        TextView txt_view_beat_name;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
