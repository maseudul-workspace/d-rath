package com.webinfotech.itcdeliverytrackingapp.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.itcdeliverytrackingapp.AndroidApplication;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.Executor;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.MainThread;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.CheckLoginInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.impl.CheckLoginInteractorImpl;
import com.webinfotech.itcdeliverytrackingapp.domain.models.UserInfo;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.LoginPresenter;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.itcdeliverytrackingapp.repository.AppRepositoryImpl;

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, CheckLoginInteractor.Callback {

    Context mContext;
    LoginPresenter.View mView;
    AndroidApplication androidApplication;
    CheckLoginInteractorImpl checkLoginInteractor;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkLogin(String mobile, String password) {
        checkLoginInteractor = new CheckLoginInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, mobile, password);
        checkLoginInteractor.execute();
    }

    @Override
    public void onLoginSuccess(UserInfo userInfo) {
        mView.hideLoader();
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        Toast.makeText(mContext, "Login Successfull", Toast.LENGTH_SHORT).show();
        mView.onLoginSuccess();
    }

    @Override
    public void onLoginFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
