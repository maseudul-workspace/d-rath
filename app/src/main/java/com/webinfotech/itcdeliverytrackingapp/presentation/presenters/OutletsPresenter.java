package com.webinfotech.itcdeliverytrackingapp.presentation.presenters;

import com.webinfotech.itcdeliverytrackingapp.presentation.ui.adapters.OutletAdapter;

public interface OutletsPresenter {
    void fetchOutletLists();
    void deliverOutlet(String address, String latitude, String longitud);
    interface View {
        void showLoader();
        void hideLoader();
        void loadAdapter(OutletAdapter adapter);
        void onDeliverClicked();
    }
}
