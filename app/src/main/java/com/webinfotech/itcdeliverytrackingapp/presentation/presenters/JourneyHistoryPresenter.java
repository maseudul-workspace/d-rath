package com.webinfotech.itcdeliverytrackingapp.presentation.presenters;

public interface JourneyHistoryPresenter {
    void endJourney(
            String endLatitude,
            String endLongitude,
            String startAddress,
            String totalKm
    );
    interface View {
        void showLoader();
        void hideLoader();
        void onEndJourneySuccess();
    }
}
