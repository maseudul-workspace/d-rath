package com.webinfotech.itcdeliverytrackingapp.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.itcdeliverytrackingapp.AndroidApplication;
import com.webinfotech.itcdeliverytrackingapp.R;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyStartData;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.JourneyStartPresenter;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.impl.JourneyStartPresenterImpl;
import com.webinfotech.itcdeliverytrackingapp.presentation.ui.adapters.BeatAdapter;
import com.webinfotech.itcdeliverytrackingapp.presentation.ui.adapters.VehicleAdapter;
import com.webinfotech.itcdeliverytrackingapp.presentation.ui.services.LocationService;
import com.webinfotech.itcdeliverytrackingapp.threading.MainThreadImpl;
import com.webinfotech.itcdeliverytrackingapp.util.GetLocationClass;
import com.webinfotech.itcdeliverytrackingapp.util.Helper;

import java.io.IOException;
import java.util.List;

public class JourneyStartActivity extends AppCompatActivity implements JourneyStartPresenter.View, GetLocationClass.Callback {

    @BindView(R.id.recycler_view_beat)
    RecyclerView recyclerViewBeat;
    @BindView(R.id.recycler_view_vehicles)
    RecyclerView recyclerViewVehicles;
    @BindView(R.id.txt_view_vehicle_name)
    TextView txtViewVehicleName;
    @BindView(R.id.txt_view_beat_name)
    TextView txtViewBeatName;
    ProgressDialog progressDialog;
    JourneyStartPresenterImpl mPresenter;
    GetLocationClass getLocationClass;
    int beatId = 0;
    int vehicleId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey_start);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Start Journey");
        initialisePresenter();
        initialiseLocationClass();
        setUpProgressDialog();
        mPresenter.getJourneyFormData();
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new JourneyStartPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void initialiseLocationClass() {
        getLocationClass = new GetLocationClass(this, this::onLocationGettingSuccess);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loadData(BeatAdapter beatAdapter, VehicleAdapter vehicleAdapter) {
        recyclerViewBeat.setAdapter(beatAdapter);
        recyclerViewBeat.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewBeat.setNestedScrollingEnabled(false);
        recyclerViewVehicles.setAdapter(vehicleAdapter);
        recyclerViewVehicles.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewVehicles.setNestedScrollingEnabled(false);
    }

    @OnClick(R.id.layout_beat) void onBeatLayoutClicked() {
        if (recyclerViewBeat.getVisibility() == View.VISIBLE) {
            recyclerViewBeat.setVisibility(View.GONE);
        } else {
            recyclerViewBeat.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.layout_vehicle) void onVehicleLayoutClicked() {
        if (recyclerViewVehicles.getVisibility() == View.VISIBLE) {
            recyclerViewVehicles.setVisibility(View.GONE);
        } else {
            recyclerViewVehicles.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_start_journey) void onJourneyStartClicked() {
        if (beatId == 0) {
            Toast.makeText(this, "Please Select Beat", Toast.LENGTH_SHORT).show();
        } else if (vehicleId == 0) {
            Toast.makeText(this, "Please Select Vehicle", Toast.LENGTH_SHORT).show();
        } else {
            showLoader();
            getLocationClass.getCurrentLocation();
        }
    }

    @Override
    public void onBeatClicked(String beatName, int beatId) {
        txtViewBeatName.setText(beatName);
        this.beatId = beatId;
        recyclerViewBeat.setVisibility(View.GONE);
    }

    @Override
    public void onVehicleClicked(String vehicleName, int vehicleId) {
        this.vehicleId = vehicleId;
        txtViewVehicleName.setText(vehicleName);
        recyclerViewVehicles.setVisibility(View.GONE);
    }

    @Override
    public void onJourneyStartSuccess(JourneyStartData journeyStartData) {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setJourneyStartData(this, journeyStartData);
        Intent i = new Intent(this, LocationService.class);
        startService(i);
        Intent intent = new Intent(this, OutletsActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onJourneyStartFail() {

    }

    @Override
    public void onLocationGettingSuccess(Location location) {
        Log.e("LogMsg", "Location Getting Success");
        getLocationClass.stopLocationUpdates();
        try {
            List<Address> addressList = Helper.getAddressFromLatLang(this, location);
            String address = addressList.get(0).getAddressLine(0) + ", " + addressList.get(0).getLocality() + ", " + addressList.get(0).getAdminArea();
            mPresenter.startJourney(beatId, vehicleId, Double.toString(location.getLatitude()), Double.toString(location.getLongitude()), address);
        } catch (IOException e) {
            hideLoader();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

}
