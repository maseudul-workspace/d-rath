package com.webinfotech.itcdeliverytrackingapp.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.itcdeliverytrackingapp.AndroidApplication;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.Executor;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.MainThread;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.EndJourneyInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.impl.EndJourneyInteractorImpl;
import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyStartData;
import com.webinfotech.itcdeliverytrackingapp.domain.models.UserInfo;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.JourneyHistoryPresenter;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.itcdeliverytrackingapp.repository.AppRepositoryImpl;
import com.webinfotech.itcdeliverytrackingapp.util.Helper;

public class JourneyHistoryPresenterImpl extends AbstractPresenter implements JourneyHistoryPresenter, EndJourneyInteractor.Callback {

    Context mContext;
    JourneyHistoryPresenter.View mView;
    EndJourneyInteractorImpl endJourneyInteractor;
    AndroidApplication androidApplication;

    public JourneyHistoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void endJourney(String endLatitude, String endLongitude, String startAddress, String totalKm) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        JourneyStartData journeyStartData = androidApplication.getJourneyStartData(mContext);
        if (userInfo != null && journeyStartData != null) {
            endJourneyInteractor = new EndJourneyInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, journeyStartData.journeyId, Helper.getCurrentDate(), Helper.getCurrentTime(), endLatitude, endLongitude, startAddress, totalKm);
            endJourneyInteractor.execute();
        }
    }

    @Override
    public void onEndJourneySuccess() {
        mView.hideLoader();
        mView.onEndJourneySuccess();
    }

    @Override
    public void onEndJourneyFail(String errroMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errroMsg, Toast.LENGTH_SHORT).show();
    }
}
