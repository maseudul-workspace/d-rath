package com.webinfotech.itcdeliverytrackingapp.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.itcdeliverytrackingapp.AndroidApplication;
import com.webinfotech.itcdeliverytrackingapp.R;
import com.webinfotech.itcdeliverytrackingapp.presentation.ui.services.LocationService;
import com.webinfotech.itcdeliverytrackingapp.util.GetLocationClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
{

    private static final int ERROR_DIALOG_REQUEST = 9001;
    String[] appPremisions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    GetLocationClass getLocationClass;
    private BroadcastReceiver broadcastReceiver;
    @BindView(R.id.txt_view_main)
    TextView txtViewMain;
    @BindView(R.id.btn_start)
    Button btnStart;
    @BindView(R.id.btn_stop)
    Button btnStop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }


    public boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String perm : appPremisions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
//               Initialise map
            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        this.showAlertDialog("", "This app needs read and write storage permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    } else {
                        this.showAlertDialog("", "You have denied some permissions. Allow all permissions to download file at [Setting] > [Permissions]",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }

                }
            }
        }
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (broadcastReceiver ==  null) {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
                    txtViewMain.setText("Distance: " + androidApplication.getKm(getApplicationContext()));
                    if (androidApplication.getStatus(getApplicationContext())) {
                        btnStart.setVisibility(View.GONE);
                        btnStop.setVisibility(View.VISIBLE);
                    } else {
                        btnStart.setVisibility(View.VISIBLE);
                        btnStop.setVisibility(View.GONE);
                    }
                }
            };
        }
        registerReceiver(broadcastReceiver, new IntentFilter("Location Update"));
    }

    @OnClick(R.id.btn_start) void onBtnStartClicked() {
        if (checkAndRequestPermissions()) {
            Intent i = new Intent(this, LocationService.class);
            startService(i);
        }
    }

    @OnClick(R.id.btn_stop) void onBtnStopClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setDistanceTravelledDistanceTo(this, 0, false);
        Intent i = new Intent(this, LocationService.class);
        stopService(i);
    }

}
