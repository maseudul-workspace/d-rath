package com.webinfotech.itcdeliverytrackingapp.presentation.presenters;

public interface LoginPresenter {
    void checkLogin(String mobile, String password);
    interface View {
        void showLoader();
        void hideLoader();
        void onLoginSuccess();
    }
}
