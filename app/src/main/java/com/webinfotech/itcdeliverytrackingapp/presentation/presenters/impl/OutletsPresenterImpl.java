package com.webinfotech.itcdeliverytrackingapp.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.itcdeliverytrackingapp.AndroidApplication;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.Executor;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.MainThread;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.DeliverOutletInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.FetchOutletListInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.impl.DeliverOutletInteractorImpl;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.impl.FetchOutletListInteractorImpl;
import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyStartData;
import com.webinfotech.itcdeliverytrackingapp.domain.models.OutletList;
import com.webinfotech.itcdeliverytrackingapp.domain.models.UserInfo;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.OutletsPresenter;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.itcdeliverytrackingapp.presentation.ui.adapters.OutletAdapter;
import com.webinfotech.itcdeliverytrackingapp.repository.AppRepositoryImpl;

import javax.xml.transform.sax.TemplatesHandler;

public class OutletsPresenterImpl extends AbstractPresenter implements OutletsPresenter, FetchOutletListInteractor.Callback, OutletAdapter.Callback, DeliverOutletInteractor.Callback {

    Context mContext;
    OutletsPresenter.View mView;
    FetchOutletListInteractorImpl fetchOutletListInteractor;
    AndroidApplication androidApplication;
    DeliverOutletInteractorImpl deliverOutletInteractor;
    int outletId;

    public OutletsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchOutletLists() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        JourneyStartData journeyStartData = androidApplication.getJourneyStartData(mContext);
        if (userInfo != null && journeyStartData != null) {
            fetchOutletListInteractor = new FetchOutletListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, journeyStartData.beatId, userInfo.id);
            fetchOutletListInteractor.execute();
        }
    }

    @Override
    public void deliverOutlet(String address, String latitude, String longitud) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        JourneyStartData journeyStartData = androidApplication.getJourneyStartData(mContext);
        if (userInfo != null && journeyStartData != null) {
            deliverOutletInteractor = new DeliverOutletInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, journeyStartData.journeyId, outletId, address, latitude, longitud);
            deliverOutletInteractor.execute();
        }
    }

    @Override
    public void onGettingOutletListSuccess(OutletList[] outletLists) {
        OutletAdapter outletAdapter = new OutletAdapter(mContext, outletLists, this);
        mView.loadAdapter(outletAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingOutletListFail(String errroMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errroMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onOutletClicked(int outletId) {
        mView.showLoader();
        this.outletId = outletId;
        mView.onDeliverClicked();
    }

    @Override
    public void onOutletDeliverSuccess() {
        mView.hideLoader();
        Toast.makeText(mContext, "Delivery Successfull", Toast.LENGTH_SHORT).show();
        fetchOutletLists();
    }

    @Override
    public void onOutletDeliverFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, "Delivery Successfull", Toast.LENGTH_SHORT).show();
    }
}
