package com.webinfotech.itcdeliverytrackingapp.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.itcdeliverytrackingapp.R;
import com.webinfotech.itcdeliverytrackingapp.domain.models.Vehicles;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.ViewHolder> {

    public interface Callback {
        void onVehicleClicked(int vehicleId, String vehicleName);
    }
    Context mContext;
    Vehicles[] vehicles;
    Callback mCallback;

    public VehicleAdapter(Context mContext, Vehicles[] vehicles, Callback mCallback) {
        this.mContext = mContext;
        this.vehicles = vehicles;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_vehicle_name, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewVehicleName.setText(vehicles[position].name);
        holder.layoutVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onVehicleClicked(vehicles[position].id, vehicles[position].name);
            }
        });
        holder.txtViewPerKmCost.setText(vehicles[position].kmCost);
    }

    @Override
    public int getItemCount() {
        return vehicles.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_vehicle_name)
        TextView txtViewVehicleName;
        @BindView(R.id.txt_view_per_km_cost)
        TextView txtViewPerKmCost;
        @BindView(R.id.layout_vehicle)
        View layoutVehicle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
