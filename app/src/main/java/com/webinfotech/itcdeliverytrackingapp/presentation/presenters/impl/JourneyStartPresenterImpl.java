package com.webinfotech.itcdeliverytrackingapp.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.itcdeliverytrackingapp.AndroidApplication;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.Executor;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.MainThread;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.GetJourneyFormDataInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.StartJourneyInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.impl.GetJourneyFormDataInteractorImpl;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.impl.StartJourneyInteractorImpl;
import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyFormData;
import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyStartData;
import com.webinfotech.itcdeliverytrackingapp.domain.models.UserInfo;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.JourneyStartPresenter;
import com.webinfotech.itcdeliverytrackingapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.itcdeliverytrackingapp.presentation.ui.adapters.BeatAdapter;
import com.webinfotech.itcdeliverytrackingapp.presentation.ui.adapters.VehicleAdapter;
import com.webinfotech.itcdeliverytrackingapp.repository.AppRepositoryImpl;
import com.webinfotech.itcdeliverytrackingapp.util.Helper;

import androidx.appcompat.view.menu.MenuView;

public class JourneyStartPresenterImpl extends AbstractPresenter implements JourneyStartPresenter, GetJourneyFormDataInteractor.Callback, BeatAdapter.Callback, VehicleAdapter.Callback, StartJourneyInteractor.Callback {

    Context mContext;
    JourneyStartPresenter.View mView;
    AndroidApplication androidApplication;
    GetJourneyFormDataInteractorImpl getJourneyFormDataInteractor;
    StartJourneyInteractorImpl startJourneyInteractor;

    public JourneyStartPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getJourneyFormData() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        getJourneyFormDataInteractor = new GetJourneyFormDataInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken);
        getJourneyFormDataInteractor.execute();
    }

    @Override
    public void startJourney(int beatId, int vehicleId, String startLatitude, String startLongitude, String startAddress) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            startJourneyInteractor = new StartJourneyInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.apiToken, userInfo.id, beatId, vehicleId, Helper.getCurrentDate(), Helper.getCurrentTime(), startLatitude, startLongitude, startAddress);
            startJourneyInteractor.execute();
        }
    }

    @Override
    public void onGettingFormDataSuccess(JourneyFormData journeyFormData) {
        BeatAdapter beatAdapter = new BeatAdapter(mContext, journeyFormData.beats, this::onBeatClicked);
        VehicleAdapter vehicleAdapter = new VehicleAdapter(mContext, journeyFormData.vehicles, this::onVehicleClicked);
        mView.loadData(beatAdapter, vehicleAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingFormDataFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBeatClicked(int beatId, String beatName) {
        mView.onBeatClicked(beatName, beatId);
    }

    @Override
    public void onVehicleClicked(int vehicleId, String vehicleName) {
        mView.onVehicleClicked(vehicleName, vehicleId);
    }

    @Override
    public void onJourneyStartSuccess(JourneyStartData journeyStartData) {
        mView.hideLoader();
        Toast.makeText(mContext, "Journey Start Success", Toast.LENGTH_SHORT).show();
        mView.onJourneyStartSuccess(journeyStartData);
    }

    @Override
    public void onJourneyStartFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.onJourneyStartFail();
    }
}
