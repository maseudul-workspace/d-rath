package com.webinfotech.itcdeliverytrackingapp;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyStartData;
import com.webinfotech.itcdeliverytrackingapp.domain.models.UserInfo;

public class AndroidApplication extends Application {

    UserInfo userInfo;
    JourneyStartData journeyStartData;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setDistanceTravelledDistanceTo(Context context, int km, boolean isRunning) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_ITC_APP_PREFERENCES), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("distance", km);
        editor.putBoolean("isRunning", isRunning);
        editor.commit();
    }

    public boolean getStatus(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_ITC_APP_PREFERENCES), Context.MODE_PRIVATE);
        return sharedPref.getBoolean("isRunning", false);
    }

    public int getKm(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_ITC_APP_PREFERENCES), Context.MODE_PRIVATE);
        return sharedPref.getInt("distance", 0);
    }

    public void setUserInfo(Context context, UserInfo userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_ITC_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString(getString(R.string.KEY_USER_DETAILS), new Gson().toJson(userInfo));
        } else {
            editor.putString(getString(R.string.KEY_USER_DETAILS), "");
        }

        editor.commit();
    }

    public UserInfo getUserInfo(Context context){
        UserInfo user;
        if(userInfo != null){
            user = userInfo;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_ITC_APP_PREFERENCES), Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString(getString(R.string.KEY_USER_DETAILS),"");
            if(userInfoJson.isEmpty()){
                user = null;
            }else{
                try {
                    user = new Gson().fromJson(userInfoJson, UserInfo.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

    public void setJourneyStartData(Context context, JourneyStartData journeyStartData) {
        this.journeyStartData = journeyStartData;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_ITC_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(journeyStartData != null) {
            editor.putString(getString(R.string.KEY_JOURNEY_START_DATA), new Gson().toJson(journeyStartData));
        } else {
            editor.putString(getString(R.string.KEY_JOURNEY_START_DATA), "");
        }

        editor.commit();
    }

    public JourneyStartData getJourneyStartData(Context context) {
        JourneyStartData journeyStartData1;
        if(journeyStartData != null){
            journeyStartData1 = journeyStartData;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_ITC_APP_PREFERENCES), Context.MODE_PRIVATE);
            String journeyStartDataJson = sharedPref.getString(getString(R.string.KEY_JOURNEY_START_DATA),"");
            if(journeyStartDataJson.isEmpty()){
                journeyStartData1 = null;
            }else{
                try {
                    journeyStartData1 = new Gson().fromJson(journeyStartDataJson, JourneyStartData.class);
                    this.journeyStartData = journeyStartData1;
                }catch (Exception e){
                    journeyStartData1 = null;
                }
            }
        }
        return journeyStartData1;
    }

}
