package com.webinfotech.itcdeliverytrackingapp.repository;

import android.util.Log;

import com.google.gson.Gson;
import com.webinfotech.itcdeliverytrackingapp.domain.models.CommonResponse;
import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyFormDataWrapper;
import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyStartDataWrapper;
import com.webinfotech.itcdeliverytrackingapp.domain.models.OutletListWrapper;
import com.webinfotech.itcdeliverytrackingapp.domain.models.UserInfoWrapper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.Header;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = ApiClient.createService(AppRepository.class);
    }

    public UserInfoWrapper checkLogin(String mobile, String password) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkLogin(mobile, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
            Log.e("LogMsg", "Error: " + e.getMessage());
        }
        return userInfoWrapper;
    }

    public JourneyFormDataWrapper getJourneyFormData(String apiToken) {
        Log.e("LogMsg", "Api Token: " + apiToken);
        JourneyFormDataWrapper journeyFormDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getJourneyFormData("Bearer " + apiToken);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    journeyFormDataWrapper = null;
                }else{
                    journeyFormDataWrapper = gson.fromJson(responseBody, JourneyFormDataWrapper.class);
                }
            } else {
                journeyFormDataWrapper = null;
            }
        }catch (Exception e){
            journeyFormDataWrapper = null;
        }
        return journeyFormDataWrapper;
    }

    public JourneyStartDataWrapper startJourney(String authorization,
                                                int userId,
                                                int beatId,
                                                int vehicleId,
                                                String startDate,
                                                String startTime,
                                                String startLatitude,
                                                String startLongitude,
                                                String startAddress) {

        JourneyStartDataWrapper journeyStartDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> start = mRepository.startJourney("Bearer " + authorization, userId, beatId, vehicleId, startDate, startTime, startLatitude, startLongitude, startAddress);

            Response<ResponseBody> response = start.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    journeyStartDataWrapper = null;
                }else{
                    journeyStartDataWrapper = gson.fromJson(responseBody, JourneyStartDataWrapper.class);
                }
            } else {
                journeyStartDataWrapper = null;
            }
        }catch (Exception e){
            journeyStartDataWrapper = null;
            Log.e("LogMsg", "Error: " + e.getMessage());
        }
        return journeyStartDataWrapper;
    }

    public OutletListWrapper fetchOutlist(String authorization,
                                          int beatId,
                                          int userId
                                          ) {
        OutletListWrapper outletListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getOutletList("Bearer " + authorization, beatId, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    outletListWrapper = null;
                }else{
                    outletListWrapper = gson.fromJson(responseBody, OutletListWrapper.class);
                }
            } else {
                outletListWrapper = null;
            }
        }catch (Exception e){
            outletListWrapper = null;
            Log.e("LogMsg", "Error: " + e.getMessage());
        }
        return outletListWrapper;
    }

    public CommonResponse endJourney(String authorization,
                                     int userId,
                                     int journeyId,
                                     String endDate,
                                     String endTime,
                                     String endLatitude,
                                     String endLongitude,
                                     String startAddress,
                                     String totalKm) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> end = mRepository.stopJourney("Bearer " + authorization, userId, journeyId, endDate, endTime, endLatitude, endLongitude, startAddress, totalKm);

            Response<ResponseBody> response = end.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Error: " + e.getMessage());
        }
        return commonResponse;
    }

    public CommonResponse deliverOutlet(String authorization,
                                        int userId,
                                        int journeyId,
                                        int outLetId,
                                        String address,
                                        String latitude,
                                        String longitude) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> deliver = mRepository.deliverOutlet("Bearer " + authorization, userId, journeyId, outLetId, address, latitude, longitude);

            Response<ResponseBody> response = deliver.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Error: " + e.getMessage());
        }
        return commonResponse;
    }

}
