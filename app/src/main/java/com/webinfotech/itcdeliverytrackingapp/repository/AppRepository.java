package com.webinfotech.itcdeliverytrackingapp.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AppRepository {

    @POST("user/login")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(@Field("mobile") String phoneNumber,
                                  @Field("password") String password
    );

    @GET("journey/form/load")
    Call<ResponseBody> getJourneyFormData(@Header("Authorization") String authorization);

    @POST("journey/start")
    @FormUrlEncoded
    Call<ResponseBody> startJourney(@Header("Authorization") String authorization,
                                    @Field("user_id") int userId,
                                    @Field("beat_id") int beatId,
                                    @Field("vehicle_id") int vehicleId,
                                    @Field("start_date") String startDate,
                                    @Field("start_time") String startTime,
                                    @Field("start_latitude") String startLatitude,
                                    @Field("start_longtitude") String startLongitude,
                                    @Field("start_address") String startAddress
    );

    @GET("outlet/list/{beat_id}/{user_id}")
    Call<ResponseBody> getOutletList(@Header("Authorization") String authorization,
                                     @Path("beat_id") int beatId,
                                     @Path("user_id") int userId
                                     );


    @POST("journey/end")
    @FormUrlEncoded
    Call<ResponseBody> stopJourney(@Header("Authorization") String authorization,
                                    @Field("user_id") int userId,
                                    @Field("journey_id") int journeyId,
                                    @Field("end_date") String endDate,
                                    @Field("end_time") String endTime,
                                    @Field("end_latitude") String endLatitude,
                                    @Field("end_longtitude") String endLongitude,
                                    @Field("end_address") String startAddress,
                                    @Field("total_km") String totalKm

    );

    @POST("outlet/delivered")
    @FormUrlEncoded
    Call<ResponseBody> deliverOutlet(@Header("Authorization") String authorization,
                                     @Field("user_id") int userId,
                                     @Field("journey_id") int journeyId,
                                     @Field("out_let_id") int outLetId,
                                     @Field("address") String address,
                                     @Field("latitude") String latitude,
                                     @Field("longtitude") String longitude
    );

}
