package com.webinfotech.itcdeliverytrackingapp.domain.interactors.impl;

import com.webinfotech.itcdeliverytrackingapp.domain.executors.Executor;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.MainThread;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.EndJourneyInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.models.CommonResponse;
import com.webinfotech.itcdeliverytrackingapp.repository.AppRepositoryImpl;

public class EndJourneyInteractorImpl extends AbstractInteractor implements EndJourneyInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int userId;
    int journeyId;
    String endDate;
    String endTime;
    String endLatitude;
    String endLongitude;
    String startAddress;
    String totalKm;

    public EndJourneyInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int userId, int journeyId, String endDate, String endTime, String endLatitude, String endLongitude, String startAddress, String totalKm) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.userId = userId;
        this.journeyId = journeyId;
        this.endDate = endDate;
        this.endTime = endTime;
        this.endLatitude = endLatitude;
        this.endLongitude = endLongitude;
        this.startAddress = startAddress;
        this.totalKm = totalKm;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onEndJourneyFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onEndJourneySuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.endJourney(authorization, userId, journeyId, endDate, endTime, endLatitude, endLongitude, startAddress, totalKm);
        if (commonResponse == null) {
            notifyError("Something Went Wrong", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.loginError);
        } else {
            postMessage();
        }
    }
}
