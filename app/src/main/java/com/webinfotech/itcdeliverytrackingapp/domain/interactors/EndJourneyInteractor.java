package com.webinfotech.itcdeliverytrackingapp.domain.interactors;

public interface EndJourneyInteractor {
    interface Callback {
        void onEndJourneySuccess();
        void onEndJourneyFail(String errroMsg, int loginError);
    }
}
