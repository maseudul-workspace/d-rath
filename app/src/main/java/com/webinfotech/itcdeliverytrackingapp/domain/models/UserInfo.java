package com.webinfotech.itcdeliverytrackingapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfo {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public String mobile;

    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("api_token")
    @Expose
    public String apiToken;

}
