package com.webinfotech.itcdeliverytrackingapp.domain.interactors.impl;

import com.webinfotech.itcdeliverytrackingapp.domain.executors.Executor;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.MainThread;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.GetJourneyFormDataInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyFormData;
import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyFormDataWrapper;
import com.webinfotech.itcdeliverytrackingapp.repository.AppRepositoryImpl;

public class GetJourneyFormDataInteractorImpl extends AbstractInteractor implements GetJourneyFormDataInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;

    public GetJourneyFormDataInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingFormDataFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(JourneyFormData journeyFormData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingFormDataSuccess(journeyFormData);
            }
        });
    }



    @Override
    public void run() {
        final JourneyFormDataWrapper journeyFormDataWrapper = mRepository.getJourneyFormData(apiToken);
        if (journeyFormDataWrapper == null) {
            notifyError("Something Went Wrong", 0);
        } else if (!journeyFormDataWrapper.status) {
            notifyError(journeyFormDataWrapper.message, journeyFormDataWrapper.loginError);
        } else {
            postMessage(journeyFormDataWrapper.journeyFormData);
        }
    }
}
