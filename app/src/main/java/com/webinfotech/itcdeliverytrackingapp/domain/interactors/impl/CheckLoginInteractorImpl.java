package com.webinfotech.itcdeliverytrackingapp.domain.interactors.impl;

import android.widget.CheckBox;

import com.webinfotech.itcdeliverytrackingapp.domain.executors.Executor;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.MainThread;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.CheckLoginInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.models.UserInfo;
import com.webinfotech.itcdeliverytrackingapp.domain.models.UserInfoWrapper;
import com.webinfotech.itcdeliverytrackingapp.repository.AppRepository;
import com.webinfotech.itcdeliverytrackingapp.repository.AppRepositoryImpl;

public class CheckLoginInteractorImpl extends AbstractInteractor implements CheckLoginInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String mobile;
    String password;

    public CheckLoginInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String mobile, String password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.mobile = mobile;
        this.password = password;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.checkLogin(mobile, password);
        if (userInfoWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!userInfoWrapper.status) {
            notifyError(userInfoWrapper.message);
        } else {
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
