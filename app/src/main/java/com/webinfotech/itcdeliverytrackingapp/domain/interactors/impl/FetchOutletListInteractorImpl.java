package com.webinfotech.itcdeliverytrackingapp.domain.interactors.impl;

import com.webinfotech.itcdeliverytrackingapp.domain.executors.Executor;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.MainThread;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.FetchOutletListInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.models.OutletList;
import com.webinfotech.itcdeliverytrackingapp.domain.models.OutletListWrapper;
import com.webinfotech.itcdeliverytrackingapp.repository.AppRepositoryImpl;

public class FetchOutletListInteractorImpl extends AbstractInteractor implements FetchOutletListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int beatId;
    int userId;

    public FetchOutletListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int beatId, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.beatId = beatId;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOutletListFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(OutletList[] outletLists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOutletListSuccess(outletLists);
            }
        });
    }

    @Override
    public void run() {
        final OutletListWrapper outletListWrapper = mRepository.fetchOutlist(apiToken, beatId, userId);
        if (outletListWrapper == null) {
            notifyError("Something Went Wrong", 0);
        } else if (!outletListWrapper.status) {
            notifyError(outletListWrapper.message, outletListWrapper.loginError);
        } else {
            postMessage(outletListWrapper.outletLists);
        }
    }
}
