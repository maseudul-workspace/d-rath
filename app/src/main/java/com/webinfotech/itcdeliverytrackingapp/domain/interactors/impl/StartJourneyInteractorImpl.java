package com.webinfotech.itcdeliverytrackingapp.domain.interactors.impl;

import android.content.Context;

import com.webinfotech.itcdeliverytrackingapp.domain.executors.Executor;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.MainThread;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.StartJourneyInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyStartData;
import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyStartDataWrapper;
import com.webinfotech.itcdeliverytrackingapp.repository.AppRepositoryImpl;

public class StartJourneyInteractorImpl extends AbstractInteractor implements StartJourneyInteractor
{

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String authorization;
    int userId;
    int beatId;
    int vehicleId;
    String startDate;
    String startTime;
    String startLatitude;
    String startLongitude;
    String startAddress;

    public StartJourneyInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, String authorization, int userId, int beatId, int vehicleId, String startDate, String startTime, String startLatitude, String startLongitude, String startAddress) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.authorization = authorization;
        this.userId = userId;
        this.beatId = beatId;
        this.vehicleId = vehicleId;
        this.startDate = startDate;
        this.startTime = startTime;
        this.startLatitude = startLatitude;
        this.startLongitude = startLongitude;
        this.startAddress = startAddress;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onJourneyStartFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(JourneyStartData journeyStartData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onJourneyStartSuccess(journeyStartData);
            }
        });
    }


    @Override
    public void run() {
        final JourneyStartDataWrapper journeyStartDataWrapper = mRepository.startJourney(authorization, userId, beatId, vehicleId, startDate, startTime, startLatitude, startLongitude, startAddress);
        if (journeyStartDataWrapper == null) {
            notifyError("Something Went Wrong", 0);
        } else if (!journeyStartDataWrapper.status) {
            notifyError(journeyStartDataWrapper.message, journeyStartDataWrapper.loginError);
        } else {
            postMessage(journeyStartDataWrapper.journeyStartData);
        }
    }
}
