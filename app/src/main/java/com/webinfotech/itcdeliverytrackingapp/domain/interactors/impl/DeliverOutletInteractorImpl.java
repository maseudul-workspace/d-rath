package com.webinfotech.itcdeliverytrackingapp.domain.interactors.impl;

import com.webinfotech.itcdeliverytrackingapp.domain.executors.Executor;
import com.webinfotech.itcdeliverytrackingapp.domain.executors.MainThread;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.DeliverOutletInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.itcdeliverytrackingapp.domain.models.CommonResponse;
import com.webinfotech.itcdeliverytrackingapp.repository.AppRepositoryImpl;

public class DeliverOutletInteractorImpl extends AbstractInteractor implements DeliverOutletInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int userId;
    int journeyId;
    int outLetId;
    String address;
    String latitude;
    String longitud;

    public DeliverOutletInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int userId, int journeyId, int outLetId, String address, String latitude, String longitud) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.userId = userId;
        this.journeyId = journeyId;
        this.outLetId = outLetId;
        this.address = address;
        this.latitude = latitude;
        this.longitud = longitud;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOutletDeliverFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOutletDeliverSuccess();
            }
        });
    }

    @Override
    public void run() {
        CommonResponse commonResponse = mRepository.deliverOutlet(authorization, userId, journeyId, outLetId, address, latitude, longitud);
        if (commonResponse == null) {
            notifyError("Something Went Wrong", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.loginError);
        } else {
            postMessage();
        }
    }
}
