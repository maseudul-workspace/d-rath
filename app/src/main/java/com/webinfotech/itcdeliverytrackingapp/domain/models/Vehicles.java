package com.webinfotech.itcdeliverytrackingapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vehicles {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("km_cost")
    @Expose
    public String kmCost;

}
