package com.webinfotech.itcdeliverytrackingapp.domain.interactors.base;

/**
 * Created by Raj on 03-01-2019.
 */

public interface Interactor {
    /**
     * This is the main method that starts an interactor. It will make sure that the interactor operation is done on a
     * background thread.
     */
    void execute();
}
