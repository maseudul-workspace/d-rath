package com.webinfotech.itcdeliverytrackingapp.domain.interactors;

import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyStartData;

public interface StartJourneyInteractor {
    interface Callback {
        void onJourneyStartSuccess(JourneyStartData journeyStartData);
        void onJourneyStartFail(String errorMsg, int loginError);
    }
}
