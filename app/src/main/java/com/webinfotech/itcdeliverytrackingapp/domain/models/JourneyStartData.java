package com.webinfotech.itcdeliverytrackingapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JourneyStartData {

    @SerializedName("journey_id")
    @Expose
    public int journeyId;

    @SerializedName("beat_id")
    @Expose
    public int beatId;

}
