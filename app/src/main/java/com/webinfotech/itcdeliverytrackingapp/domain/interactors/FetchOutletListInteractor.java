package com.webinfotech.itcdeliverytrackingapp.domain.interactors;

import com.webinfotech.itcdeliverytrackingapp.domain.models.OutletList;

public interface FetchOutletListInteractor {
    interface Callback {
        void onGettingOutletListSuccess(OutletList[] outletLists);
        void onGettingOutletListFail(String errroMsg, int loginError);
    }
}
