package com.webinfotech.itcdeliverytrackingapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JourneyFormData {

    @SerializedName("vehicle_type")
    @Expose
    public Vehicles[] vehicles;

    @SerializedName("beat_name")
    @Expose
    public Beat[] beats;

}
