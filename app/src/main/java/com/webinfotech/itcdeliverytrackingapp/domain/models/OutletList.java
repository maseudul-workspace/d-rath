package com.webinfotech.itcdeliverytrackingapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OutletList {

    @SerializedName("id")
    @Expose
    public int outletId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("sify_id")
    @Expose
    public String sifyId;

    @SerializedName("beat_id")
    @Expose
    public int beatId;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("ds_name")
    @Expose
    public String dsName;

    @SerializedName("ds_type")
    @Expose
    public String dsType;

    @SerializedName("del_status")
    @Expose
    public int delStatus;

}
