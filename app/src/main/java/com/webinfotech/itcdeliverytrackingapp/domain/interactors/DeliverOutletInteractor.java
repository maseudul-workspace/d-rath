package com.webinfotech.itcdeliverytrackingapp.domain.interactors;


public interface DeliverOutletInteractor{
    interface Callback {
        void onOutletDeliverSuccess();
        void onOutletDeliverFail(String errorMsg, int loginError);
    }
}
