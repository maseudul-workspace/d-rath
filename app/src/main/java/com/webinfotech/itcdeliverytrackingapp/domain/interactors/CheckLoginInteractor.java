package com.webinfotech.itcdeliverytrackingapp.domain.interactors;

import com.webinfotech.itcdeliverytrackingapp.domain.models.UserInfo;

public interface CheckLoginInteractor {
    interface Callback {
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
