package com.webinfotech.itcdeliverytrackingapp.domain.interactors;

import com.webinfotech.itcdeliverytrackingapp.domain.models.JourneyFormData;

public interface GetJourneyFormDataInteractor {
    interface Callback {
        void onGettingFormDataSuccess(JourneyFormData journeyFormData);
        void onGettingFormDataFail(String errorMsg, int loginError);
    }
}
