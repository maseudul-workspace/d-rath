package com.webinfotech.itcdeliverytrackingapp.util;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by Raj on 03-04-2019.
 */

public class GetLocationClass implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener{


    LocationManager locationManager;
    Boolean gps_enabled = false;
    Boolean network_enabled = false;
    FusedLocationProviderClient fusedLocationProviderClient;
    GoogleApiClient googleApiClient;
    Location lastLocation;
    LocationRequest locationRequest;
    int REQUEST_CHECK_SETTINGS = 1000;

    public interface Callback{
        void onLocationGettingSuccess(Location location);
    }
    Context mContext;
    Callback mCallback;
    private static String TAG = "errorMsg";

    public GetLocationClass(Context mContext, Callback mCallback) {
        this.mContext = mContext;
        this.mCallback = mCallback;
    }

    public void getCurrentLocation(){
        try {
            Log.e("LogMsg", "Get Location ");
            googleApiClient = new GoogleApiClient.Builder(mContext)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            locationRequest = LocationRequest.create();
            locationRequest.setInterval(5000);

            locationRequest.setFastestInterval(5000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

//            checkForLocationRequestSetting(locationRequest);
            googleApiClient.connect();

        } catch (SecurityException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {

//            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
//
//            Log.e("LogMsg", "last location " + lastLocation.getLongitude());

            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);


//            if (lastLocation != null) {
//                Toast.makeText(mContext, "Latitiude: " + lastLocation.getLongitude() + " Longitude: " + lastLocation.getLatitude(), Toast.LENGTH_SHORT).show();
//                mCallback.onLocationGettingSuccess(lastLocation);
//            }

        } catch (SecurityException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("LogMsg", "Connection Failed " + connectionResult.getErrorMessage());
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("LogMsg", "Latitude: " + location.getLatitude());
        Log.e("LogMsg", "Longitude: " + location.getLongitude());
        mCallback.onLocationGettingSuccess(location);
    }

    public void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

//    private void checkForLocationRequestSetting(LocationRequest locationRequest) {
//        try {
//            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
//                    .addLocationRequest(locationRequest);
//
//            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
//
//            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
//                @Override
//                public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
//                    final Status status = locationSettingsResult.getStatus();
//                    final LocationSettingsStates locationSettingsStates = locationSettingsResult.getLocationSettingsStates();
//
//                    switch (status.getStatusCode()) {
//                        case LocationSettingsStatusCodes.SUCCESS:
//                            // All location settings are satisfied. The client can
//                            // initialize location requests here.
//                            Log.e("ErrorMsg", "onResult: SUCCESS");
//                            break;
//                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                            Log.e("ErrorMsg", "onResult: RESOLUTION_REQUIRED");
//                            // Location settings are not satisfied, but this can be fixed
//                            // by showing the user a dialog.
//                            try {
//                                // Show the dialog by calling startResolutionForResult(),
//                                // and check the result in onActivityResult().
//                                status.startResolutionForResult(
//                                        mActivity,
//                                        REQUEST_CHECK_SETTINGS);
//                            } catch (IntentSender.SendIntentException e) {
//                                // Ignore the error.
//                            }
//                            break;
//                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                            Log.d("ErrorMsg", "onResult: SETTINGS_CHANGE_UNAVAILABLE");
//                            // Location settings are not satisfied. However, we have no way
//                            // to fix the settings so we won't show the dialog.
//                            break;
//                    }
//
//                }
//            });
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }

}
